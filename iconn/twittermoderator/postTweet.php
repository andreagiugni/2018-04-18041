<?php
require_once("config.php");
require_once (constant('INCLUDE')."header.php"); 
require_once (constant('INCLUDE')."clsTweets.php"); 

$thunder=new Thunder($db,$CURRENT_LANGUAGE );
$tweet=new Tweet($db,$CURRENT_LANGUAGE );
$page=$thunder->curPageName();


if(($_GET['idtweeting']))
    $IDTWEETINGSESSION = $_GET['idtweeting'];
else
    $IDTWEETINGSESSION = 0;
    
$smarty->assign('meetings_id', $MEETING);   
$smarty->assign('rooms_id', $ROOM );  
$smarty->assign('sections_id', $QUESTIONGROUP); 
$smarty->assign('langs_id', $QUESTIONLANGUAGE );   
 $smarty->assign('tweetingsession_id', $IDTWEETINGSESSION );     

$smarty->assign('lang',$GLOBALS['LANG']);  
$smarty->assign('browser',$GLOBALS['BROWSER']);    
$smarty->assign('meeting', $thunder->GetMeetingName($MEETING));  
$smarty->assign('room', $thunder->GetRoomName($ROOM));  
$smarty->assign('session', $thunder->GetSessionName($QUESTIONGROUP));
$smarty->assign('active02',"active");	
 
$smarty->assign('title_site',"Twitter");
//$smarty->assign('title_page',"Overview");
$smarty->assign('path_css',constant('CSS'));              
$smarty->assign('path_js',constant('JS'));
$smarty->assign('action',$ACTION);	

$id=$tweet->GetIDTweetingSession($MEETING, $ROOM, $QUESTIONGROUP);

$smarty->assign('url',"meeting=".$MEETING."&room=".$ROOM."&questionlanguage=".$QUESTIONLANGUAGE."&questionGroup=".$QUESTIONGROUP."&idtweeting=".$id);	


//Intestazioni pagina  
$smarty->assign('header', constant('TEMPLATE')."header.tpl");  
//menu
 $smarty->assign('menu', constant('TEMPLATE')."menu.tpl");    
	                        
$smarty->caching = false;
$page=$thunder->curPageName();
$smarty->display(constant('TEMPLATE')."$page.tpl");
?>




