<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
   <link rel="stylesheet" href="{{$path_css}}base.css" type="text/css" />
   <link rel="stylesheet" href="{{$path_css}}fonts.css" type="text/css" />
   {{$meeting_css}}
   <script src="{{$path_js}}libs/modernizr-2.0.6.min.js"></script>
</head>
<body>
<div id="container"></div>
<div id="wrapper">
	<div id="timer"></div>
</div>
<form id="Commands" name="Commands" method="get" >
<input type="hidden" name="meeting" id="meeting" value="{{$meetings_id}}" />
<input type="hidden" name="room" id="room" value="{{$rooms_id}}" />
<input type="hidden" name="questionlanguage" id="questionlanguage"  value="{{$langs_id}}" />
<input type="hidden" name="questionGroup" id="questionGroup" value="{{$sections_id}}" />
<input type="hidden" name="command"  id="command" value="{{$command}}" />
</form>
<script src="{{$path_js}}libs/jquery-1.5.1.min.js" type="text/javascript"></script>
<!--<script src="javascript/jquery.fittext.js"></script>-->
<!--<script src="javascript/jquery.progressbar.min.js" type="text/javascript"></script>-->
<script src="javascript/swfobject.js" type="text/javascript"></script>
<script type="text/JavaScript">
$(document).ready(function(){
							 
	var timer=5000;   //3 sec.
	var int01 = setInterval(on_air, timer);
	
	var flashvars = {};
	var params = {wmode : "transparent"};
	var attributes = {};	
	swfobject.embedSWF("images/Timer3x4_04_blu.swf", "timer", "200", "150", "9.0.0", "images/expressInstall.swf", flashvars, params, attributes);

	
	function on_air()
    {		
		var meeting=$("#meeting").val();
		var room=$("#room").val();
		var questionlanguage=$("#questionlanguage").val();
		var questionGroup=$("#questionGroup").val();
		
   		$.ajax({
    			type : 'GET',
    			url : 'onair.php',    			    		
    			data: {
					meeting:meeting, room:room, questionGroup:questionGroup,questionLanguage:questionlanguage
    			},
                dataType: "html",
    			success : function(data,stato){		
				//  alert("Data Loaded: " + data);		
                   $('#container').html(data);      
    			},
    			error : function(XMLHttpRequest, textStatus, errorThrown) {
					//alert("Data Loaded: " + errorThrown);		
					$('#container').html("Nessun dato recuperato!");                    
    			}
    	});		
  		return false;
	}; 								 	    
		
  });
</script> 
</body>
</html>
