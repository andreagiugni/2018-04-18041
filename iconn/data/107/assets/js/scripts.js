function isValidMail(par_email)
{
    mailvalid=false;    
    maildomain = "";
    
    if(par_email.length>0)
    {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

        if(pattern.test(par_email))
        {
            if(maildomain!='')
            {
                if(par_email.indexOf(maildomain)!== -1)
                {
                    mailvalid=true;                
                }
            }
            else
            {
                /*
                se non è stato specificato un dominio la mail è valida
                */
                mailvalid=true; 
            }

        }
    }
    return mailvalid;
}
function renderRating(){
    var irating = $(".box-stars").attr("rating");
    $(".box-stars span").removeClass("active");
    for(var i=0; i<irating; i++){
        $(".box-stars span:nth-child("+(i+1)+")").addClass("active");
    }
}
$(function(){
    renderRating();
    $("#refresh-btn").on("tap", function(){
        location.reload();
    });
    $("#btn-be-maven").on("tap", function(){
        $("#intro").fadeOut(300);
    });
    $(".btn-home").on("tap", function(){
        $(this).closest(".screen-section").fadeOut(300);
    });
    $("#btn-programma").on("tap", function(){
        $("#programma").fadeIn(300);
    });
    $("#btn-faculty").on("tap", function(){
        $("#faculty").fadeIn(300);
    });
    $("#btn-qa").on("tap", function(){
        $("#twitter").fadeIn(300);
    });
    $(".box-stars span").on("tap",function(){
        var id=$(this).attr("id").charAt(5);
        $(".box-stars").attr("rating", id);
        renderRating();
    });
    $(".btn-presentazioni").on("tap", function(){       
        if(!$(this).hasClass("opened")){
            $(this).addClass("opened");
            $("#askpresentation").slideDown(300);
        }
        else
        {
            $(this).removeClass("opened");
            $("#askpresentation").slideUp(300);
        }
    });
    
    $("#btn-rating-session").on("tap", function(){
        //salvataggio rating sessione
        $("#popup-session").hide();
    });
    $("#btnSendRequestPresentation").on("tap",function(){
        if(isValidMail($("#emailtosave").val())){
             $.ajax({
                method: 'POST',
                url: 'http://'+location.host+'/iconn/phpservices/saveUserForPresentationsRequest.php',
                //dataType: 'json',
                cache: false,
                data:{ 
                    device: $_GET["device"],
                    email: $("#emailtosave").val(),
                    meeting : _MEETING_
                    },
                success: function(data){
                    if(data==1)
                    {
                        $("#requestpanel").hide();
                        $("#thanksforrequest").show();
                        $("#thanksforrequest p").text("La richiesta delle presentazioni è stata registrata correttamente!");
                    }
                    else
                    {
                        $("#requestpanel").hide();
                        $("#thanksforrequest").show();
                        $("#thanksforrequest p").text("Richiesta presentazioni già registrata!");
                    }
                    $("html, body").animate({ scrollTop: 0 }, "fast");
                    
//                    alert(JSON.stringify(data));
                    
//                    alert(JSON.stringify(data))
//                    var ws=[];
//                    ws["rooms"]=data;
//                    wsevent('chooseRoom_' + _ROOM_, ws);
                },
                 error: function(data){
                 }
            });
        }
	});
    
});