<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name='apple-mobile-web-app-capable' content='yes'>
        <meta name='apple-touch-fullscreen' content='yes'>
        <meta name="viewport" content="width=device-width, user-scalable=no" />    
        <link rel="stylesheet" href="assets/css/style.css">        
        <script>
            var _SCREEN_ = false;
        </script>
        <script src="assets/js/jquery-3.3.1.min.js"></script>     
        <script src="assets/js/jquery-ui.js"></script>   
        <script src="assets/js/touchy.js"></script>
        <script src="assets/js/jquery.ui.touch-punch.js"></script>
        <script src="assets/js/autobahn.min.js"></script>
        <script src="assets/js/qs.js"></script>
        <script src="assets/js/MEETING_SETTINGS.js"></script>
        <script src="assets/js/question.js"></script>
        <script src="assets/js/libs.js"></script>
        <script src="assets/js/ws.js"></script>
        <script src="faculty.js"></script>
    </head>
    <body>
        <div id="refresh-btn"></div>
        <div id="conn-status"></div>
        <!--<div id="loader">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>-->
        <!--************INTRO SCREEN************-->
        <section id="intro" class="screen-section">
            <img src="assets/img/logo-home.png" class="img-responsive logo-intro" />
            <img src="assets/img/date-intro.png" class="img-responsive date-intro" />
            <div class="clear"></div>
            <div class="box-intro">
                <p class="font-futura-medium color-grey">
                    Gentile Dottore,<br>
                    benvenuto nella <span class="color-blue font-futura-heavy">città dei Maven</span> dove si apre un nuovo orizzonte per il trattamento della sclerosi multipla. <br>
                    Due giorni per approfondire e condividere le <span class="color-blue font-futura-heavy">evidenze</span> di un percorso iniziato tempo fa, frutto di ricerca, dedizione e passione.<br> 
                    La invitiamo a percorrere insieme questa strada.<br><br>
                    <span class="color-blue font-futura-heavy" id="btn-be-maven">BE MAVEN</span>
                </p>
            </div>
            <img src="assets/img/logo-merck.png" class="logo-merck">
        </section>
        <!--************HOME SCREEN************-->
        <section id="home" class="screen-section">
            <img src="assets/img/logo-home.png" class="img-responsive logo-home" />
            <div class="box-btn-home text-center">
                <button id="btn-programma" class="font-futura-medium color-white"><span>PROGRAMMA</span></button>
                <button id="btn-faculty" class="font-futura-medium color-white"><span>FACULTY</span></button>
                <button id="btn-materiali" class="font-futura-medium color-white"><span>RICHIEDI MATERIALI</span></button>
                <button id="btn-informazioni" class="font-futura-medium color-white"><span>INFORMAZIONI</span></button>
                <button id="btn-qa" class="font-futura-medium color-white"><span>FAI UNA DOMANDA</span></button>
                <button id="btn-survey" class="font-futura-medium color-white disabled"><span>SURVEY FINALE</span></button>
            </div>
            <img src="assets/img/logo-merck.png" class="logo-merck" />
        </section>

        <!--************PROGRAMMA SCREEN************-->
        <section id="programma" class="screen-section internal">
            <header>
                <button class="btn-home"></button>
                <p class="color-white float-right font-futura-medium">PROGRAMMA</p>
            </header>
            <footer>
                <button class="btn-home color-white">HOME</button>
            </footer>
        </section>

        <!--************FACULTY SCREEN************-->
        <section id="faculty" class="screen-section internal">
           <?php include_once("template-part/faculty.php");?>
        </section>

        <!--************MATERIALI SCREEN************-->
        <section id="materiali" class="screen-section internal">
            
        </section>

        <!--************INFORMAZIONI SCREEN************-->
        <section id="informazioni" class="screen-section internal">
           
        </section>
        <!--***********TWITTER SCREEN************-->
        <section id="twitter" class="screen-section internal">
            <header>
                <div class="nav-header bg-blue">
                    <button class="back-btn"></button>
                </div>
                <img src="assets/img/logo-action.png" class="logo-header">
            </header>
            <div class="content-internal">
                <h1 class="section-title qa color-blue font-futura-medium">Q&A</h1>                
                <div class="content">
                    <div id="tweetsList"></div>
                    <div id="btnAddTweet" class="font-futura-medium color-white bg-blue">+</div>
                    <div id="newTweet">
                        <p class="color-cyano font-futura-medium padding-left-internal padding-right-internal">Desidera inviare al relatore un commento o una domanda?</p>
                        <div class="content-text-area">
                            <textarea placeholder="Scriva la sua domanda qui" rows="4" cols="50" class="font-futura-medium"></textarea>
                            <div class="btn-container">
                                <button id="send-twitter-btn" class="btn-twitter font-futura-medium color-white bg-cyano">
                                    INVIA AL RELATORE
                                    <span class="bg-white"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--***********QUESTION SCREEN************-->
        <section id="question" class="screen-section internal">
            <header>
                <div class="nav-header bg-blue"></div>
                <img src="assets/img/logo-action.png" class="logo-header">
            </header>
            <div class="content-internal">
                <h1 class="section-title question color-blue font-futura-medium">DOMANDA</h1>
                <p class="question-text color-cyano font-futura-medium padding-left-internal padding-right-internal"></p>
                <div class="content">
                    <div class="question-box padding-left-internal">
                        <div class="question-box-type question-caso-clinico">
                            <div class="d-table">
                                <span class="color-blue font-futura-medium float-left">RISPOSTA</span>
                                <hr class="bg-grey float-left" />
                            </div>
                            <ol>
                            </ol>
                        </div>
                        <div class="question-box-type question-duello">
                            <div id="slider-answer">
                            </div>
                            <div class="answer-circle-container">

                            </div>
                        </div>
                    </div>
                    <div class="thankyou-box font-futura-medium">
                        <img src="assets/img/icon-thankyou.png">
                        <p class="text-center color-white font-futura-medium">IL VOTO <br>&Egrave; STATO REGISTRATO<br><br>GRAZIE</p>
                    </div>
                    <div class="btn-container">
                        <button id="send-answer-btn" class="btn-answer font-futura-medium color-white bg-cyano disabled">
                            INVIA
                        </button>
                    </div>
                </div>
            </div>
        </section>
        <section id="popup-session" class="screen-section">
            <div class="box-popup bg-white">
                <p class="text-center font-futura-medium color-black">
                    La tua opinione è molto preziosa.<br>
                    Valuta l'ultima sessione ascoltata
                </p>
                <div class="box-stars" rating="3">
                    <span id="star-1"></span>
                    <span id="star-2"></span>
                    <span id="star-3"></span>
                    <span id="star-4"></span>
                    <span id="star-5"></span>
                </div>
                <button class="btn-circle" id="btn-rating-session">INVIA</button>
            </div>
        </section>
    </body>
    <script src="assets/js/twitter.js"></script>
    <script src="assets/js/scripts.js"></script>
</html>