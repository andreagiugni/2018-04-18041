<?php
 
error_reporting(E_ALL);
ini_set('display_errors', 1);

$SHOW_SELECTORS = '1';
$EDIT = '0';

include("header.php");

$FORM_INCLUDE = "";
	
?>


<script>

var list_on_store;
    
$(document).ready(function(){
    
    //$("#meetingandroom").attr("action","");
    
    $('#msg_to_push').jqte();
	
	// settings of status
//	var jqteStatus = true;
//	$(".status").click(function()
//	{
//		jqteStatus = jqteStatus ? false : true;
//		$('.jqte-test').jqte({"status" : jqteStatus})
//	});
    
    list_on_store =  localStorage.getItem("push");
    if(list_on_store)
    {    
        list_on_store = JSON.parse(list_on_store);
    
        for(i=0;i<=countProperties(list_on_store.pushnotifications)-1;i++)
        {
            $("#pushlist").append("<div class='pushitem' onclick='retrive(" + i + ")'>"+ (i+1) + " - " +list_on_store.pushnotifications[i]+"</div>" );
        }        
    }

});  
    
function countProperties(obj) {
  var prop;
  var propCount = 0;

  for (prop in obj) {
    propCount++;
  }
  return propCount;
}
    
function addToPushList()
{     
    if($("#msg_to_push").val()!= "")
    {
        if(list_on_store)
        {
            list_on_store.pushnotifications[countProperties(list_on_store.pushnotifications)] = $("#msg_to_push").val();        
            strListaAggiornata = JSON.stringify(list_on_store);
        }
        else
        {
            strListaAggiornata ='{"pushnotifications": { "0":"'+$("#msg_to_push").val()+'" }}';
            list_on_store = JSON.parse(strListaAggiornata);
        }

        localStorage.setItem("push",strListaAggiornata );
        $("#pushlist").append("<div class='pushitem'>"+countProperties(list_on_store.pushnotifications)+" - "+$("#msg_to_push").val()+"</div>" );

        pushform.submit();
    }
}
    
function retrive(push_index)
{
        
    $("#msg_to_push").jqteVal(list_on_store.pushnotifications[push_index]);
    
}
function clearAll()
{
    localStorage.setItem("push","");
    $("#pushlist").empty();
    list_on_store = null;
}
    
</script>

<style type="text/css">

.push_area_separator
{
    display: block;
    position: relative;
    width: 100%;
    height: 10px;
    border-bottom: 2px dotted silver;
}

.push_title
{
    display: block;
    position: relative;
    width: 100%;
    height: 50px;
    text-align: center;
    font-size: 32px;
    color:#000;
}

.msg_area
{
    display: block;
    position: relative;
    width: 90%;
    height: 400px; 
    margin:0 auto;
}
    
.tool_area
{
    display: block;
    position: relative;
    width: 50%;
    margin: 0 auto;
}

    
.btnsend
{
    background-color: silver;
    width: 100%;
    margin-top: 10px;
    cursor: pointer;
    line-height: 58px;
    vertical-align: middle;
    height: 60px;
}

.btnsend:active
{
    background-color: darkgray;
    color:#FFF;
    width: 100%;
    margin-top: 10px;
    cursor: pointer;

}
.jqte
{
    width: 70%;
    margin: 0 auto;
}
   
#pushlist
{
    display: block;
    position: relative;
    float: left;
    width: 100%;
    margin-top:20px;
}
.pushitem
{
    display: block;
    position: relative;
    float: left;
    width: 80%;
    margin-left: 10%;
    margin-right: 10%;
    background-color: silver;
    padding: 3px;
    margin-bottom: 2px;
}
#pushtitle
{
    display: block;
    position: relative;
    float: left;
    width: 100%;
    text-align: center;
}

#pushtools
{
    display: block;
    position: relative;
    float: left;
    width: 100%;
}
.clearall
{
    text-align: center;
    color:red;
    width:200px;
    height: 30px;
    background-color: #FFF;
    border: 1px solid silver;
    margin:0 auto;
    cursor: pointer;
    line-height: 30px;
}
</style>




<?php

//echo $ACTION;

if($ACTION=="SENDALL")  {

    $entryData= array( 
				'meeting' => $MEETING, //$MEETING,
				'room' =>  $ROOM, //$ROOM
				'service' => 'pushnotification_'.$ROOM, //*** Importante ***
				'wsData' => array( 
					"msg"=>$_GET['msg_to_push']
					)
				);
    
    sendWebSocket($entryData);	
    
    //$("#msg_to_push").val()
}

if($ACTION=="STOP")  {

    $entryData= array( 
				'meeting' => $MEETING, //$MEETING,
				'room' =>  $ROOM, //$ROOM
				'service' => 'flush_'.$ROOM //*** Importante ***				
				);
    
    sendWebSocket($entryData);	
        
}


echo "<form id=\"pushform\" action='".$_SERVER['PHP_SELF']."' method='get'>".$MEETINGANDROOM_INCLUDE.
    "<input type='hidden' name='pushmsg' value='dfsd fsd fsdfsdfsdf sd'>";

?>


<div class='push_area_separator'></div>

<div class='push_title'>MESSAGE TO PUSH</div>

<textarea id="msg_to_push" name="msg_to_push" class="msg_area"></textarea>

<div class='push_area_separator'></div>

<div class="tool_area">
    <?php
   
    echo "<input name=\"action\" type=\"submit\" value=\"SENDALL\" class=\"btnsend push_title\" id=\"btnSendToAll\" onclick=\"addToPushList();\"/>";
    echo "<input name=\"action\" type=\"submit\" value=\"STOP\" class=\"btnsend push_title\" id=\"btnSendToAll\" onclick=\"pushform.submit();\"/>";
    //echo "<div name=\"action\" value=\"SENDALL\" class=\"btnsend push_title\" id=\"btnSendToAll\" onclick=\"addToPushList();\"></div>";
    ?>
    
</div>

<div id="pushtitle"><h4>Notifiche già inviate</h4></div>
<div id="pushtools"><h4><div class='clearall' onclick='clearAll();'>Cancella la lista</div></h4></div>
<div id="pushlist"></div>

</form>

