<?php
namespace WsService;
use Ratchet\ConnectionInterface;
//use Ratchet\MessageComponentInterface;  
use Ratchet\Wamp\WampServerInterface;
use React\EventLoop\LoopInterface;




error_reporting(E_ALL);
ini_set('display_errors', 1);



	
class Pusher implements WampServerInterface{//, MessageComponentInterface
	/*
      A lookup of all the topics clients have subscribed to
     */
	private $clients;
	

    public function __construct()//$db
    {
        //$this->db = $db;
		$this->clients = new \SplObjectStorage;
    }
	
	
	private $lastEntry= array();
	private $maxIn=0;
	
    protected $subscribedServices = array();

    public function onSubscribe(ConnectionInterface $conn, $topic) {
		//system('cls');
		//echo "\nNewIn - Numero clients: ".count($this->subscribedServices);
        $this->subscribedServices[$topic->getId()] = $topic;
    }

    /*
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
    public function onServiceCall($entry) {

    	$entryData = json_decode($entry, true);
 		if(!isset($entryData["wsData"]))
        	$entryData["wsData"]="";



        if(!isset($entryData['saveAsLastCommand'])||$entryData['saveAsLastCommand']){
        	//$this->db->Execute("DELETE from tblSocketNotification WHERE fkIdMeeting = ".$entryData['meeting']." AND fkIdRoom =".$entryData['room']." INSERT into tblSocketNotification VALUES ('".base64_encode($entry)."',".$entryData['meeting'].",".$entryData['room'].",'".$entry."')"); //".base64_decode($entry)."
    		global $lastEntry;		
			$lastEntry["room_".$entryData["room"]]=$entry;
    	}

        // If the lookup topic object isn't set there is no one to publish to
        if (!array_key_exists($entryData['service'], $this->subscribedServices)) {
            return;
        }

        $topic = $this->subscribedServices[$entryData['service']];

       

		// re-send the data to all the clients subscribed to that service
		$topic->broadcast($entryData["wsData"]);

    }

    public function onUnSubscribe(ConnectionInterface $conn, $topic) {
    }
	
	
    public function onOpen(ConnectionInterface $conn) {
		$this->clients->attach($conn);
		echo "\nIn: ".$conn->resourceId;
		
		global $maxIn;
		if(count($this->clients)>$maxIn) $maxIn=count($this->clients);
		
		echo "\nNumero utenti connessi: ".count($this->clients)." (MAX:".$maxIn.")";
		
		
    }
	
	public function onMessage(ConnectionInterface $from, $msg) {
		foreach ($this->clients as $client) {
			$client->send($msg);
		}
	}
	
    public function onClose(ConnectionInterface $conn) {
		//echo "\nDisconnection: ".$conn-> remoteAddress." (#Clients: ".count($this->clients).")";
		echo "\nOut: ".$conn->resourceId;
		$this->clients->detach($conn);
		echo "\n\nNumero utenti connessi: ".count($this->clients);
    }

    public function onCall(ConnectionInterface $conn, $id, $topic, array $params) {
		global $lastEntry;
		$room=$topic->getId();
    	if(is_array($lastEntry)||is_object($lastEntry)){
			if(isset($lastEntry["room_".$room])){
		        $lastData = json_decode($lastEntry["room_".$room], true);
				if (count($lastData)>0) {
					if(!isset($lastData["wsData"])) $lastData["wsData"]="";
					$conn->event($lastData['service'], $lastData["wsData"]);
				}
			}
		}
    }

    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible) {
        // In this application if clients send data it's because the user hacked around in console
         echo "\n\n*****onPublish****** ";
        $conn->close();
    }
    public function onError(ConnectionInterface $conn, \Exception $e) {
    	 echo "\n\n*****onError****** ";
		$conn->close();
    }
	
}