<?php

include("connect.php");
include("language.php");
include("config.php");

	
	if(isset($_GET['expscreen']))
		$S1 = $_GET['expscreen'];
	else
		$S1 = "";
	
	if(isset($_GET['expapproved']))
		$S2 = $_GET['expapproved'];
	else
		$S2 = "";
	
	if(isset($_GET['exptoapprove']))
		$S3 = $_GET['exptoapprove'];
	else
		$S3 = "";
	
	if(isset($_GET['autorefresh']))
		$autorefresh = $_GET['autorefresh'];
	else
		$autorefresh = "";

function monthbyid($id) {

global $CURRENT_LANGUAGE;

if($CURRENT_LANGUAGE == "en") {
	
	switch($id) {
	
	case  1: return "gennaio";
	case  2: return "febbraio";
	case  3: return "marzo";
	case  4: return "aprile";
	case  5: return "maggio";
	case  6: return "giugno";
	case  7: return "luglio";
	case  8: return "agosto";
	case  9: return "settembre";
	case 10: return "ottobre";
	case 11: return "novembre";
	case 12: return "dicembre";
	
	}
}
/*
if($CURRENT_LANGUAGE == "en") {
	
	switch($id) {
	
	case  1: return "january";
	case  2: return "february";
	case  3: return "march";
	case  4: return "april";
	case  5: return "may";
	case  6: return "june";
	case  7: return "july";
	case  8: return "august";
	case  9: return "september";
	case 10: return "october";
	case 11: return "november";
	case 12: return "december";
	
	}
}
*/
}

function formatdate($date) {

global $CURRENT_LANGUAGE;

if($CURRENT_LANGUAGE == "it") {

	if($date <> "") {
	
	
		$year = substr($date,0, 4);
		$month = substr($date,5, 2);
		$day = (int)substr($date, 8, 2);
		$timex = substr($date,13);
		$hour = (int)substr($date, 11, 2)+5;
	//	return $day." ".monthbyid($month)." ".$year." alle ".$hour.$timex;
		
		return $day." ".monthbyid($month)." alle ".$hour.$timex;
	}
	else
		return "";
}

if($CURRENT_LANGUAGE == "en") {

	if($date <> "") {
	
	
		$year = substr($date,0, 4);
		$month = substr($date,5, 2);
		$day = (int)substr($date, 8, 2);
		$timex = substr($date,13);
		$hour = (int)substr($date, 11, 2)+1;
		//return monthbyid($month)." ".$day.", ".$year.", ".$hour.$timex;
		return monthbyid($month)." ".$day.", ".$hour.$timex;

	}
	else
		return "";
}


}

function GetUserNameForDevice($id) {

global $USE_DEVICE_ID;
global $db;

	if($USE_DEVICE_ID) {
		return "iPod ".$id;
	}
	else {
		$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");	
	
		if($row) 
			return $row['slastname']." ".$row['sfirstname'];
		else
			return "iPod non assegnato";
	}
}


function GetUserIdForDevice($id) {

global $db;

	$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");	
	
	if($row)
		return $row['pkiduser'];
	else
		return "";
}

function GetRoomNameFromId($id) {

global $db;

	$row = $db->GetRow("SELECT * FROM tblroomentity WHERE pkidroom  = '".$id."'");	
	
	if($row)
		return $row['sname'];
	else
		return "";
}

function GetUserNameForUserId($id, $self=0) {

global $db;

	if($id == $self)
		return translate('List:You');
		
	$row = $db->GetRow("SELECT * FROM tbluserentity WHERE pkiduser  = '".$id."'");	
	
	if($row) 
		return $row['sfirstname']." ".$row['slastname'];
	else
		return "";
}
	
	
function ShowSender($sender) {

global $ANONYMOUS;
global $USE_DEVICE_ID;

	if($ANONYMOUS) 
			echo '<span class="comment">';
		else {
			if($USE_DEVICE_ID) 
				echo '<span class="comment">'.translate('List:From').
					': '.GetUserNameForDevice($sender).'</span>';
			else 
				echo '<span class="comment">'.translate('List:From').
					': '.GetUserNameForUserId($sender).'</span>';
		}	
}

function ShowDate($date) {

global $ANONYMOUS;

	if($ANONYMOUS)
		echo '<span class="comment"><br>'./*translate('List:Date').": ".*/formatdate($date).'</span>';
	else
		echo '<span class="comment">'./*translate('List:Date').": ".*/formatdate($date).'</span>';
		
	// echo '<span class="starcomment"></span>';
}	


function strictify ( $string ) {
     
	$fixed = htmlspecialchars( $string, ENT_QUOTES );
	
    $trans_array = array();
    for ($i=127; $i<255; $i++) {
        $trans_array[chr($i)] = "&#" . $i . ";";
    }

    $really_fixed = strtr($fixed, $trans_array);

    return $really_fixed;
}


$quit = 0;
	
if(isset($_GET['you'])) {
	
	//echo "USING CACHE";
	
	$YOU = $_GET['you'];
	$SESSION = $_GET['session'];
	$MODERATOR = $_GET['moderator'];
	$MEETING = $_GET['meeting'];
	
	//if(isset($_GET['lang']))
	//	$LANG = $_GET['lang'];
	//else
		$LANG = "en-GB";
		
	
	if(strlen($LANG  != 2))
	 	$CURRENT_LANGUAGE =  substr($LANG , 0, 2);
	else
		$CURRENT_LANGUAGE = $LANG;

	$ROOM = $_GET['room'];
	$DEVICE = $_GET['device'];
	$TITLE = $_GET['title'];
	
	if($_GET['you_are_speaker'] == "0)")
		$YOU_ARE_SPEAKER = 0;
	else if($_GET['you_are_speaker'] == "1)")
		$YOU_ARE_SPEAKER = 1;
	else
		$YOU_ARE_SPEAKER = $_GET['you_are_speaker'];
	
	//echo "FROM CACHE YOU ARE SPEAKER ".$YOU_ARE_SPEAKER;
}
else {

	//echo "RELODING FROM DB";
	




// E' POSSIBILE ASSOCIARE I TWEETS AL DISPOSITIVO O AGLI UTENTI


	if($USE_DEVICE_ID)
		$YOU = $_GET['device'];
	else
		$YOU = GetUserIdForDevice($_GET['device']);
	
// SE E' SPECIFICATA LA TWITTER SESSION, IGNORIAMO MEETING E ROOM

	if(isset($_GET['s'])) 
		$row = $db->GetRow("SELECT * FROM tbltweetingsessions WHERE 
						pkidTweetingSession  = '".$_GET['s']."'");	
	else
	{
	
		$z = "SELECT * FROM tblStack WHERE 
						sArgument LIKE '%twitter%' AND
						fkidRoom  = '".$_GET['room']."'
					AND fkidMeeting = '".$_GET['meeting']."' ORDER BY pkidstack DESC";
					
		$ra = $db->GetRow($z);	
		
		$ss = explode('=',$ra['sparams']);
	
		//echo $z;
		
		//echo $ra['sparams']."XXXXXXXXXXXXX".$ss[1];
		
		$q = "SELECT * FROM tbltweetingsessions WHERE 
						pkidTweetingSession  = '".$ss[1]."'";
						
		$row = $db->GetRow($q);	
		
	}
	
	
	
	
		
	
// ESTRAE I DATI DELLA TWITTER SESSION E LI TRASFORMA IN VARIABILI DI SESSIONE

	if($row) {
	
		$SESSION = $row['pkidTweetingSession'];
		$TITLE = utf8_decode($row['stitle']);
		$MODERATOR = 0;
		$MODERATED = $row['bModerated'];
		if(isset($row['fkidModeratorId']))
			$MODERATOR = $row['fkidModeratorId'];
	 
		$row = $db->GetRow("SELECT * FROM tbltweetingspeakers WHERE 
						fkidTweetingSession  = '".$SESSION."'
					AND fkidspeaker = '".$YOU."'");						
		if($row)
			$YOU_ARE_SPEAKER = 1;
		else
			$YOU_ARE_SPEAKER = 0;
			
		
	}
	else
		$quit = 1;
		
	$MEETING = $_GET['meeting'];
	$ROOM = $_GET['room'];
	$DEVICE = $_GET['device'];
	
	
	//if(isset($_GET['lang']))
	//	$LANG = $_GET['lang'];
//	else
		$LANG = "en-GB";
	
	
	
	if(strlen($LANG  != 2))
	 	$CURRENT_LANGUAGE =  substr($LANG , 0, 2);
	else
		$CURRENT_LANGUAGE = $LANG;
		 
		 
	//echo "XXXXXXXXXXXX".$CURRENT_LANGUAGE."XXXXXXXXXXX<br><br>";
	

	
	//echo "DEVICE: ".$DEVICE." MODERATOR: ".$MODERATOR." SESSION: ".$SESSION;

}

		
if(!$quit) {

		if($MODERATOR)
			$MODERATED = 1;
		else
			$MODERATED = 0;
		
	//echo "XXXXXXXXX".$MODERATED."XXXX".$MODERATED;
		
		if($YOU == $MODERATOR)
			$YOU_ARE_MODERATOR = 1;
		else
			$YOU_ARE_MODERATOR = 0;
					
		
		$FORM_INCLUDE = "
		
		<input type='hidden' name='you' value='".$YOU."'>
		<input type='hidden' name='session' value='".$SESSION."'>
		<input type='hidden' name='moderator' value='".$MODERATOR."'>
		<input type='hidden' name='meeting' value='".$MEETING."'>
		
		<input type='hidden' name='expscreen' value='".$S1."'>
		<input type='hidden' name='expapproved' value='".$S2."'>
		<input type='hidden' name='exptoapprove' value='".$S3."'>
		<input type='hidden' name='autorefresh' value='".$autorefresh."'>
		
		<input type='hidden' name='title' value='".$TITLE."'>
		<input type='hidden' name='room' value='".$ROOM."'>
		<input type='hidden' name='lang' value='".$LANG."'>
		<input type='hidden' name='you_are_speaker' value='".$YOU_ARE_SPEAKER."'>
		<input type='hidden' name='device' value='".$DEVICE."'>
		";
		
		$FORM_INCLUDE_MIDDLE = "
		
		<input type='hidden' name='you' value='".$YOU."'>
		<input type='hidden' name='session' value='".$SESSION."'>
		<input type='hidden' name='moderator' value='".$MODERATOR."'>
			<input type='hidden' name='autorefresh' value='".$autorefresh."'>
		<input type='hidden' name='meeting' value='".$MEETING."'>
		
		<input type='hidden' name='expscreen' value='".$S1."'>
		<input type='hidden' name='expapproved' value='".$S2."'>
		<input type='hidden' name='exptoapprove' value='".$S3."'>
		
		<input type='hidden' name='title' value='".$TITLE."'>
		<input type='hidden' name='room' value='".$ROOM."'>
		<input type='hidden' name='lang' value='".$LANG."'>
		
		<input type='hidden' name='you_are_speaker' value='".$YOU_ARE_SPEAKER."'>
		<input type='hidden' name='device' value='".$DEVICE."'>
		";
		
		$FORM_INCLUDE_SHORT = "
		
		<input type='hidden' name='you' value='".$YOU."'>
		<input type='hidden' name='session' value='".$SESSION."'>
		<input type='hidden' name='moderator' value='".$MODERATOR."'>
		<input type='hidden' name='meeting' value='".$MEETING."'>
		<input type='hidden' name='autorefresh' value='".$autorefresh."'>
		
		<input type='hidden' name='title' value='".$TITLE."'>
		<input type='hidden' name='lang' value='".$LANG."'>
		<input type='hidden' name='room' value='".$ROOM."'>
		<input type='hidden' name='you_are_speaker' value='".$YOU_ARE_SPEAKER."'>
		<input type='hidden' name='device' value='".$DEVICE."'>
		";
	
}

if(!$ITISINLISTCALLBACK){
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<title>Twitter <?php echo basename( $_SERVER['PHP_SELF']); ?></title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name = "viewport" content = "width = 1024,  initial-scale = 1, user-scalable = no">
    <link href="css/style.css" type="text/css" rel="stylesheet" />
    <link href="css/menu.css" type="text/css" rel="stylesheet" />
	<script src="javascript/functions.js" type="text/javascript"></script>
    <link rel="apple-touch-icon" href="homescreen.png"/>
    <link href="startup.png" rel="apple-touch-startup-image" />

<style type="text/css">


 * {
        /* 
        impostazioni flessibilità iPad      */
        -webkit-transform: translate3d(0, 0, 0); 
        -webkit-perspective: 1024;  
        -webkit-backface-visibility: hidden;
  
    
        -webkit-user-select: none;               /* disable text select */
        -webkit-touch-callout: none;              /* disable callout, image save panel (popup) */
        -webkit-tap-highlight-color: transparent; /* "turn off" link highlight */
        
        background-size: 100%;
        background-repeat: no-repeat;
        font-family: Arial Narrow, Helvetica, sans-serif;
}

 .sala {
    
    
    
display: inline-block;
    height: 35px;
    background: white;
    color: #6E6E70;
    font-weight: bold;
    text-transform: uppercase;
    line-height: 25px;
    font-size: 27px;
position: absolute;
top: 160px;
left: 45px;}
.sala::before{
    position: relative;
    width: 4px;
    height: 70%;
    float: right;
    margin-left:15px;
    background: #fec157;
    content: '';
}
.topicContainer{
    color: #038FDE;
    font-weight: bold;
    font-size: 22px;
    width:934px;
}
 .yellowSquare{
    position: relative;
    width: 13px;
    height: 13px;
    float: left;
    margin: 7px 25px 0px 10px;
    background: #FEC157;
    display:block;
     }
.names{ 
    margin-top:20px;
   text-align: center;
   }     
    
.names td span{
    color: #FEC157;  
    font-weight: bold;  
    text-transform: uppercase;  
    line-height: 30px;  
    font-size: 25px;
    text-decoration: underline;}   
.names td{
	position:relative;
    width: 50%;
    vertical-align:top;
    color: #6E6E70;
	font-size: 20px; 
	
	line-height: 31px;} 
	  
.names td::before{
	position: absolute;
	width: 250px;
	height: 106%;
	overflow-y: auto;
	background: -webkit-radial-gradient(center, ellipse cover, rgba(255, 255, 255, 1) 0%, #D9E7ED 100%);
	-webkit-overflow-scrolling: touch;
	border: 2px solid #8FC7E2;
	content: '';
	top: -10px;
	left: 106px;
	z-index: -1;
	border-radius: 25px;
	}

	
.detailes{
    position: relative;
    width:934px;
       margin-left: 45px; margin-top: 30px; margin-bottom: 50px; padding-bottom: 30px; border-bottom: 1px dashed #BBB;}




#back{
		
			-webkit-tap-highlight-color: #ffffff;
			-webkit-animation-name:pulse;
			-webkit-animation-duration:2s;
			-webkit-animation-iteration-count: infinite;
			background-image:url(Content.png);
			background-size:100%;
			width:121px;
			height:35px;
			margin:10px;
			
		}
		@-webkit-keyframes pulse {
			  0% {
				-webkit-transform: scale3d(1.0,1.0,1.0);
				-webkit-animation-timing-function: ease-in-out;
				  }
			  25% {
				-webkit-transform: scale3d(1.05,1.05,1.05);
				-webkit-animation-timing-function: ease-in-out;
			  }
			  50% {
				-webkit-transform: scale3d(1.0,1.0,1.0);
				-webkit-animation-timing-function: ease-in-out;
				  }
		}	
#btnIndietro{
			position: absolute;
			width: 200px;
			height: 95px;
			background-image: url(indietro.png);
			background-size: 137px 40px;
			background-repeat: no-repeat;
			background-position: 15px center;
			}	
		#title{
			position: absolute;
			width: 640px;
			height: 50px;
			font-size: 30px;
			color: #87C1A2;
			text-align: center;
			top: 80px;
			font-weight: bold;
			}		
		#footer{
			position:absolute;
			width:640px;
			height:324px;			
			bottom:0px;
			margin:0px;
			background-image:url(imgFooter.png);
			background-repeat:no-repeat;
			}	
        </style>
		
		</head><body>
        
        
        <!-- INIZIO MENU 
            <div id="menuContainer" >
                <div class="btnMenu">Gruppi workshop</div>
                <div class="btnMenu active" onclick="location.href='url:workshopSlides/index.html';">Osservazioni</div>
                <div class="btnMenu" onclick="location.href='url:faculty/index.html';">Faculty</div>
                <div class="btnMenu" onclick="location.href='url:agenda/index.html';">Agenda</div>   
                <div class="btnMenu_home" onclick="location.href='url:index.html';">Home</div> 
            </div>
		<!-- FINE MENU -->
 <!-- 
        <div id="btnIndietro" onclick="location.href='home:'"></div><div id="footer"></div>
        
          <div id="back" onclick="location.href='/iconn/data/62/index.html'"></div>



<div class="sala"><?php //echo $TITLE;?></div>

 <div class="loghi"></div>     <div class="logoAbilify"></div>-->
 
 <?php } ?>