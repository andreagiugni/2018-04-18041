<?php


global $CURRENT_LANGUAGE;

//echo "XXXXXXXXXXXX".$CURRENT_LANGUAGE."XXXXXXXXXXX<br><br>";

		function add_translation($country_code, $language_array) {
			
			global $CONFIG;
			
			if (!isset($CONFIG->translations))
				$CONFIG->translations = array();
							
			$country_code = strtolower($country_code);
			$country_code = trim($country_code);
			
			if (is_array($language_array) && sizeof($language_array) > 0 && $country_code != "") {
				
				if (!isset($CONFIG->translations[$country_code])) {
					$CONFIG->translations[$country_code] = $language_array;
				} else {
					$CONFIG->translations[$country_code] += $language_array;
				}
				
				return true;
				
			}
			return false;
			
		}
		
	
	
		function translate($message_key, $language = "") {
			
			global $CONFIG;
				
			global $CURRENT_LANGUAGE;
			
			//echo "XXXXXXXXXXXX".$CURRENT_LANGUAGE."XXXXXXXXXXX<br><br>";
			
			if (!$language) 
				if($CURRENT_LANGUAGE)
					$language = $CURRENT_LANGUAGE;
				else
					$language = "it";
			
			if (isset($CONFIG->translations[$language][$message_key])) {
				return $CONFIG->translations[$language][$message_key];
			} else if (isset($CONFIG->translations["it"][$message_key])) {
				return $CONFIG->translations["it"][$message_key];
			}
				
			return $message_key;
			
		}
		

$italian = array(

'Index:SessionModerated' => "Il Twitter ti permette di inviare domande ai relatori.<br> Questa sessione &eacute; moderata.<br>Tutti le domande dovranno essere approvate da un moderatore.",
		
//'Index:SessionModerated' => "I moderatori di questa sessione sono<br><br>PierFranco CONTE<br>Roberto LABIANCA<br><br>
//Lei ha l'opportunit&aacute; di inviare un quesito ai relatori selezionando \"LISTA DEI TWEETS\"",

'Index:SessionNotModerated' => "Questa sessione non &eacute; moderata. I nuovi commenti diventano immediatamente visibili a tutti gli utenti.",

'Index:TweetingSession' => "Sessione di Tweeting",

'Index:NoTweetingSessions' => '<span class="header">Attenzione</span><br>Non ci sono sessioni di tweeting attive nella sala ',
			
'Index:WarningEnterRoom' => '<span class="header">Attenzione</span><br>Per partecipare ad un tweeting &eacute; necessario prima entrare in una sala.',
	
'Index:ShowTweets' => 'LISTA DELLE DOMANDE',

'List:SendTweet' => 'INVIA UNA DOMANDA',

'List:From' => 'Da',	

'List:You' => 'Te',

'List:Date' => 'Data',

'List:To' => 'Per',

'List:Info' => 'INDIETRO',

'Tweet:Back' => 'INDIETRO',

'Tweet:Send' => 'INVIA',

'Tweet:TypeYourTweet' => 'Inserisci la tua domanda toccando l\'area sottostante',

"List:NoYet" => 'Non sono presenti domande. Clicca su "INVIA UNA DOMANDA" per inviarne una.'

);
		

$english = array(

'Index:SessionModerated' => "You can use this tool to ask questions to the speaker, to review existing questions and to share your comments with your colleagues. This session is moderated.",
		
'Index:SessionNotModerated' => "This session is not moderated. All new comments will become immediately visible by all twitter users.",

'Index:TweetingSession' => "Session",

'Index:NoTweetingSessions' => '<span class="header">Warning</span><br>There are no active tweeting session in the current room',
			
'Index:WarningEnterRoom' => '<span class="header">Warning</span><br>To participate to a tweeting session you need first to log into a room',
		
'Index:ShowTweets' => 'Show questions',

'List:SendTweet' => 'Ask a question',

'List:From' => 'From',	

'List:You' => 'You',

'List:Date' => 'Date',

'List:To' => 'To',

'List:Info' => 'Info',

'Tweet:Back' => 'BACK',

'Tweet:Send' => 'SEND',

'Tweet:TypeYourTweet' => 'Type your question',

"List:NoYet" => 'No questions yet'

	
);
	

add_translation("en",$italian);
add_translation("it",$english);	

?>