/**
 * Example external configuration file. 
 * You can freely categorize these nodes
 */ 
var conf = {
	
	// default clip configuration
	defaults: {
		
		autoPlay: true,
		autoBuffering: true,
		baseUrl: 'http://blip.tv/file/get',
		seekableOnBegin: false,
	
		// functions are also supported
		onBegin: function() {
			
			// make controlbar visible, fade lasts 4 seconds
			this.getControls().fadeIn(4000);
			
		},
		onStart: function(clip) {
//			alert("Playing");
			this.seek(5.5);
		},
		duration: 10
		
	},
	
	
	
	// my skins
	skins: {		
		gray:  {
			backgroundColor: '#666666',
			buttonColor: '#333333',
			opacity: 0,
			time: false,
			autoHide: false
		}
		
		// setup additional skins here ...		
	}
	
}

