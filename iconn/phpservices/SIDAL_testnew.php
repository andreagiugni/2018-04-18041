<?php 
include("connect.php");

function translatecontent($id, $table, $lang) {
	
	global $db;
	
	$row = NULL;
	
if($table == "questions") {

	$q22 =  "SELECT * FROM tbltranslations WHERE fkid  = '".$id."' AND stable = '".$table."' AND slanguage = '".$lang."'";
	//echo $q22;
	
	$row = $db->GetRow($q22);	
}

if($table == "answers") {

	$q33 =  "SELECT * FROM tbltranslations WHERE fkid  = '".$id."' AND stable = '".$table."' AND slanguage = '".$lang."'";
	
	//echo $q33;
	
	$row = $db->GetRow($q33);	
}

	if($row) 
		return $row['stranslation'];
	else
		return NULL;
}


function strictify ( $string ) {

	//return htmlentities($string);
	
	$fixed = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
/*
       $fixed = htmlspecialchars( $string, ENT_QUOTES );
		
       $trans_array = array();
       for ($i=127; $i<255; $i++) {
           $trans_array[chr($i)] = "&#" . $i . ";";
       }

       $really_fixed = strtr($fixed, $trans_array);

       return $really_fixed;
*/
		return $fixed;
		
   }
   
   
$out = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';

// Cerca prima tutte le azioni che riguardano solo e unicamente quel device

//$rows = $db->GetAll("SELECT * FROM tblstack WHERE fkiddevice = '".$_GET['device']."' AND bdelivered = '0' ORDER BY pkidstack ASC ");	


$q2 = "SELECT * FROM tblstack WHERE 
					(
					 ((fkidmeeting = '".$_GET['meeting']."') OR (fkidmeeting = 0)) AND  (scommand NOT LIKE 'LOGIN') AND 
					 ((fkidroom = '".$ROOM."') OR (fkidroom = 0)) AND
					 ((fkiddevice = '".$_GET['device']."') OR (fkiddevice = 0)) AND
					 (bpersistent = '1')) ORDER BY pkidstack DESC
					
					";
/*
$q2 = "SELECT * FROM tblstack WHERE 
					(
					 ((fkiddevice = '".$_GET['device']."') OR (fkiddevice = 0)) AND
					 (pkidstack NOT IN
                    	(SELECT fkidstack FROM tbldelivered WHERE fkiddevice = '".$_GET['device']."')
						)
					) order by pkidstack asc
					";
*/
//echo $q2;
	
	
$rows = $db->GetAll($q2);	
	

	
//echo $q2;
	
foreach($rows as $row) {


//echo "COMMAND: ".$row['scommand']."<br";


if(($row['scommand'] == "ASK") || ($row['scommand'] == "GRADING")) {
	
	if($row['sargument'] != "") {
		
		$row['scommand'] = "URL";
		$row['sargument'] = "data/".$_GET['meeting']."/".$row['sargument'];
		
		$q = 0;
		$count = 0;
	
		if(isset($row['sparams']))
			parse_str($row['sparams']);
		
		
		
		$rw = $db->GetRow("SELECT * FROM tblquestionevaluationentity WHERE pkidquestion = '".$q."'");
		$qws = $db->GetAll("SELECT * FROM tblanswerevaluation WHERE ngroup = '".$rw['ngroupanswer']."' ORDER BY norder ASC");
		$par = "enableAnswers=1&idQuestion=".$q."&idVoteHistory=".$count."&numMaxAnswer=".$rw['nmaxanswers']."&arrayRisposte=";
		
		$answersPar=""; 
			foreach($qws as $qw) {
				if($answersPar=="")
					$answersPar .= $qw['pkidanswer'];
				else
					$answersPar .= "-".$qw['pkidanswer'];
			}

		$par.=$answersPar;
		$row['sparams'] = $par;
		
	}
	else{
			if(isset($_GET['lang']))
					$QUESTIONLANGUAGE = $_GET['lang'];
			else
					$QUESTIONLANGUAGE = "";
				
			$q = 0;
			$count = 0;
					
			//echo $row['sparams'];
					
			if(isset($row['sparams']))
				parse_str($row['sparams']);
					
					//echo "QUESTION: ".$q."<br>";
					//echo "COUNT: ".$count."<br>";

			$rw = $db->GetRow("SELECT * FROM tblquestionevaluationentity WHERE pkidquestion = '".$q."'");
					
			if($rw['bText'] == 1) {
				$row['scommand'] = "URL";
				$row['sargument'] = "textquestions/index.php?q=".$q."&meeting=".$_GET['meeting']."&room=".$_GET['room']."&count=".$count."&device=".$_GET['device'];
				$row['sparams'] = "";
			}
			else {
				
					$translation = translatecontent($q, "questions", $QUESTIONLANGUAGE);

					if($translation)  
						$rw['squestion'] = $translation;

					$testogruppo = "";
				
					if(isset($rw['squestion']) && $rw['squestion'] != "") 
						$out_domanda = "###".$rw['nmaxanswers']."###".$rw['norder']."###".$testogruppo."###".strictify($rw['squestion'])."###";
					else {
						$out_domanda = "###1###0###Errore###DOMANDA NON TROVATA"."###";
						$row['sparams'] = "q=0&count=0";
						continue;
					}

					if(isset($rw['ngroupanswer'])) {
						$qws = $db->GetAll("SELECT * FROM tblanswerevaluation WHERE ngroup = '".$rw['ngroupanswer']."' ORDER BY norder ASC");
					
						foreach($qws as $qw) {
						
						
						
						$translation = translatecontent($qw['pkidanswer'], "answers", $QUESTIONLANGUAGE);

							if($translation)  
								$qw['sanswer'] = $translation;
								
							$out_domanda .= $qw['pkidanswer']."###".strictify($qw['sanswer'])."###";
						}
					}
					

				 // }
					$row['sargument'] = $out_domanda;
			}
		}	
		
	}

	$out .= '	<dict>
		<key>Command</key>
		<string>'.$row['scommand'].'</string>
		<key>Argument</key>
		<string><![CDATA['.$row['sargument'].']]></string>
		<key>Params</key>
		<string><![CDATA['.$row['sparams'].']]></string>
		<key>Date</key>
		<string><![CDATA['.$row['dtminsert'].']]></string>
		<key>Room</key>
		<string>'.$row['fkidroom'].'</string>
		<key>Meeting</key>
		<string>'.$row['fkidmeeting'].'</string>
		<key>KeepOn</key>
		<string>'.$row['bkeepon'].'</string>
		<key>Block</key>
		<string>'.$row['bblock'].'</string>
		</dict>';
	
	//	$result = $db->Execute("UPDATE tblstack SET bdelivered = '1' WHERE pkidstack = '".$row['pkidstack']."'");


		$qq = "INSERT into tbldelivered
					(fkiddevice, fkidstack, dtmtime)
                     VALUES ('". $_GET["device"] ."', 
					         '".$row['pkidstack']."',
						     '". date("Y-m-d H:i:s") ."')";
							 
		//echo $qq;
	
		$result = $db->Execute($qq);					

}	

$out .= '</array>
</plist>';

echo $out;

?>