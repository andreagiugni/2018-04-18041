<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	include("connect.php");

if(!isset($_GET['meeting'])||$_GET['meeting']==0||$_GET['meeting']=='')
	die("Specificare 'meeting' come parametro in querystring\n\nAssicurati di aver creato i questiongroups con i relativi nomi delle cartelle.");

	$meeting = $_GET['meeting'];
	
	$selectQuestionGroups ='select qg.pkidquestiongroup, qg.sFolderName from tblquestiongroups qg 
		join tblquestiongroupsevent_map qgm
		on qg.pkidquestiongroup = qgm.pkidquestiongroup
		where qgm.pkidevent = '.$meeting;
	
	$presentations=$db->GetAll($selectQuestionGroups);
	
	$qFill="Truncate Table [dbMeeting].[dbo].[tbl_LILLY_SlidesList]
			
			INSERT INTO [dbMeeting].[dbo].[tbl_LILLY_SlidesList]
							   ([fkIdPresentation]
							   ,[nSlideNumber]
							   ,[sSlideType]
							   ,[fkIdQuestion]
							   ,[sFilename])
						 VALUES ";
						 
	foreach($presentations as $presentation){	
		 $folder="../data/".$meeting."/".$presentation['sFolderName'];
		 if (is_dir($folder)){
		  if ($files = opendir($folder)){
			$counter =1;
			echo "<br/><br/>Esploro cartella: ".$presentation['sFolderName'];			
			while ($file = readdir($files)) {	
				if (preg_match('/.png/i',$file)||preg_match('/.jpg/i',$file)){
					echo "<br/>".$counter.") --> ".$file;
					$qFill .="(".$presentation["pkidquestiongroup"].",".$counter.",'image',NULL,'".$file."'),";
					$counter++;
				}
			}
			closedir($files);
		  }
		}else{
			echo "<br/><br/>*** ".$presentation['sFolderName']." --> Non trovato ***<br/><br/>";
			}
	}
	$result= $db->Execute(substr($qFill, 0, -1));
	if($result)
		echo "<br/><br/><br/><br/>Query eseguita: <br/><br/>".substr($qFill, 0, -1);
	else
		echo "<br/><br/>****** ATTENZIONE, LA QUERY NON HA AVUTO ESITO POSITIVO. <br/><br/> RIPROVA o CHIAMA UN TECNICO *******";
	
?>