<?php 
include("connect.php");



$out = '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<array>';

// Cerca prima tutte le azioni che riguardano solo e unicamente quel device

$rows = $db->GetAll("SELECT * FROM tblstack WHERE fkiddevice = '".$_GET['device']."' AND bdelivered = '0' ");	
	
foreach($rows as $row) {

$out .= '	<dict>
		<key>Command</key>
		<string>'.$row['scommand'].'</string>
		<key>Argument</key>
		<string><![CDATA['.$row['sargument'].']]></string>
		<key>Params</key>
		<string><![CDATA['.$row['sparams'].']]></string>
		<key>Date</key>
		<string><![CDATA['.$row['dtminsert'].']]></string>
		<key>Room</key>
		<string>'.$row['fkidroom'].'</string>
		<key>Meeting</key>
		<string>'.$row['fkidmeeting'].'</string>
	</dict>';
	
	$result = $db->Execute("UPDATE tblstack SET bdelivered = '1' WHERE pkidstack = '".$row['pkidstack']."'");

}	

// Accoda poi anche le azioni broadcast per tutta la room
// Promemoria: l'innocente codice che segue fa si che, anche se non viene trovata alcuna
// entry e non viene eseguito, la plist non venga più riconosciuta come tale!

/*
$rows2 = $db->GetAll("SELECT * FROM tblstack WHERE fkiddevice = '-1' AND 
fkidroom = '".$_GET['room']."' AND bdelivered = '0' ");	

foreach($rows2 as $row2) {

$out .= '	<dict>
		<key>Command</key>
		<string>'.$row2['scommand'].'</string>
		<key>Argument</key>
		<string><![CDATA['.$row2['sargument'].']]></string>
		<key>Params</key>
		<string><![CDATA['.$row2['sparams'].' ]]></string>
		<key>Date</key>
		<string><![CDATA['.$row2['dtminsert'].']]></string>
		<key>Room</key>
		<string>'.$row2['fkidroom'].'</string>
		<key>Meeting</key>
		<string>'.$row2['fkidmeeting'].'</string>
	</dict>';
}	
*/


$out .= '</array>
</plist>';

echo $out;

/*
if($GLOBAL->DEBUG) {
	echo "<pre>";
	print_r($db);
*/

?>