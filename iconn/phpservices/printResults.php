<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
 	include("connect.php");
 	
 	$meeting = $_GET["meeting"];
 	$room = $_GET["room"];

	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
	date_default_timezone_set('Europe/London');

	/** Include PHPExcel_IOFactory */
	require_once dirname(__FILE__) . '/PHPExcel/IOFactory.php';
	$objPHPExcel = PHPExcel_IOFactory::load("../data/".$meeting."/_resultsTmpl.xls");
	$objPHPExcel->setActiveSheetIndex(0);



 	//{"question_1":"M","question_2":">50-60","question_3":"Medico universitario","question_4":"Reumatologia","question_5":"10-30","question_6":"3","question_7":"1 - 25 %","question_8":"> 20 U/ml","question_9":"> 300 U/ml","question_10":"Sì","question_11":"Presenza di erosioni"}
	

 	$q="SELECT DISTINCT (fkIdUser), sAnswer FROM tblOpenAnswer WHERE fkIdMeeting = ".$meeting." AND fkIdroom = ".$room;
	$users = $db->GetAll($q);		

	$startingRow=4;
	

	if($users){
		foreach ($users as $user) {
			$survey = json_decode($user["sAnswer"]);
			for ($i=1; $i <= 11; $i++) { 
				$k= "question_".$i;
				// echo $survey->$k;
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$startingRow,$user["fkIdUser"]);
				$objPHPExcel->getActiveSheet()->setCellValue(chr(ord('A')+$i).($startingRow),$survey->$k);
			}
			
			//recupero risposte a domande interattive
			// $q="select 
					// pkidquestion,ivotestoconsider, pkidvotingsession,ua.fkIdUser ,ua.sanswer 
					
					// from 
						// tblhandvotingsessions v 
						// join tblquestionevaluationentity q on q.pkidquestion = v.fkidquestion 
						// left join 
							// (select 
								// e.sanswer, u.fkidquestion , u.fkIdUser , u.fkidvotehistory 
								// from tbluseranswerevaluation u 
								// left join tblanswerevaluation e on u.fkIdanswer = e.pkidanswer 
								// where u.fkIdUser = ".$user["fkIdUser"]." ) ua 
								
								// on ua.fkidvotehistory = v.pkidvotingsession 
								
						// where 
							// fkidquestiongroup = 418 
							// and ivotestoconsider > 0 
							
							// order by q.norder, ivotestoconsider";
			$q="select 
					pkidquestion,
					ivotestoconsider, 
					 pkidvotingsession,
					ua.fkIdUser ,
					ua.sanswer 
					from tblhandvotingsessions v 
						join tblquestionevaluationentity q on q.pkidquestion = v.fkidquestion 
						left join 
							(
							select e.sanswer, u.fkidquestion , u.fkIdUser , u.fkidvotehistory , u.dtmtime
							from tbluseranswerevaluation u 
							join
							(
							select  fkidquestion ,  fkIdUser , fkidvotehistory , max( dtmtime) as lastdatevode
							from tbluseranswerevaluation
							where fkiduser = ".$user["fkIdUser"]."
							group by fkidquestion ,  fkIdUser , fkidvotehistory  
							
							) as lastvote
							
							on u.fkidquestion = lastvote.fkidquestion and
							u.fkIdUser = lastvote.fkIdUser and
							u.fkidvotehistory=lastvote.fkidvotehistory and
							 u.dtmtime = lastvote.lastdatevode
							
							
							left join tblanswerevaluation e on u.fkIdanswer = e.pkidanswer 
								where u.fkIdUser = ".$user["fkIdUser"]." 
							
							) ua 
								on ua.fkidvotehistory = v.pkidvotingsession 
								where fkidquestiongroup = 418 and ivotestoconsider > 0 
								order by q.norder, ivotestoconsider";

			$interactive = $db->GetAll($q);	

			if(sizeof($interactive)!=18)
				die("Something went wrong - Please check if each question has at least one 'To be considered' voting session highlighted.<br><br/>Size of results: ".sizeof($interactive)." <br><br>Query:<br/>".$q);
			else{
				for ($i=0; $i <  18; $i+=3) { 
					// $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(ord('N')+$i, $startingRow ,$interactive[$i]["sanswer"]);
					// $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(ord('N')+$i+2, $startingRow ,$interactive[$i+1]["sanswer"]);
					// $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(ord('N')+$i+1, $startingRow ,$interactive[$i+2]["sanswer"]);

					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13+$i, $startingRow ,$interactive[$i]["sanswer"]);
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13+$i+2, $startingRow ,$interactive[$i+1]["sanswer"]);
					// $jAns = json_decode(str_replace('"', '\\"', $interactive[$i+2]["sanswer"]));
					$jAns = json_decode($interactive[$i+2]["sanswer"],true);

					// $pippo = print_r($jAns,true);
					
					$pippo = $jAns['domanda'];
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13+$i+1, $startingRow ,$pippo);
				}
			}




			$startingRow++;
		}
	}else{
		echo 'NO';
	}

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Statistica portrait - '.date("d-m-Y").'.xls"');

header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>