<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<title>Twitter</title>
<head>
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
<meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />
<link href="css/style.css" type="text/css" rel="stylesheet" />
<link rel="apple-touch-icon" href="homescreen.png"/>
<link href="startup.png" rel="apple-touch-startup-image" />
<script src="javascript/jquery-1.6.1.min.js" type="text/javascript"></script>

<style>

.selectcustom {
	font-size:20px;
	padding-top:5px;
	padding-bottom:5px;
	margin-left:10px;
	margin-top:30px;
	border:2px;
	margin:5px;
	background:#ffffff;
	
}

</style>


</head>
<script>

var LastFingerprint = "";

function checkIfRefreshIsNeeded(session) {

	var urlo = "checkIfRefreshIsNeeded.php?session="+session;
	//alert(urlo);

	$.ajax({
                type: "GET",
				url: urlo,
				success: function(msg, textStatus) {                  

					    
 					if(textStatus == "success"){      
			
						//alert(msg+" VS "+LastFingerprint);
            	
						if((LastFingerprint != "") && (msg != LastFingerprint)) {
									location.href = location.href
									//alert("NEW:" + msg + " " + LastFingerprint);
						}
						
						LastFingerprint = msg;
						
							 
						//if(msg="OK")
							//location.href = location.href; 
						
										  
                    }
                    else {
						
						//alert(urlo);
                        
						alert(textStatus);
                        alert(msg);
                    }

			    },
				error: function() {
				
						//alert(urlo);
				  //      alert("No route to host");
            
				}
				
			});
			
} 
</script>

<?php

include("connect.php");

if(isset($_GET['session']))
	$SESSION = $_GET['session'];
else
	$SESSION = 0;

if(isset($_GET['skin']))
	$SKIN = $_GET['skin'];
else
	$SKIN = 'light';

	//echo "XXXXXXXXXXXX SKIN".$SKIN;
	
if(isset($_GET['textsize']))
	$TEXTSIZE = $_GET['textsize'];
else
	$TEXTSIZE = 'medium';

switch($TEXTSIZE) {

case 'large': $smallfontsize = 5;
			   $bigfontsize = 7;
			   break;

case 'medium': $smallfontsize = 3;
			   $bigfontsize = 5;
			   break;

case 'small': $smallfontsize = 2;
			   $bigfontsize = 4;
			   break;
			   
}

switch($SKIN) {

case 'dark':  $color1 = "#ffffff";
			  $color3 = "#555555";
			  $color2 = "#000000";
			   break;

case 'light': $color1 = "#000000";
			  $color3 = "#aaaaaa";
			  $color2 = "#ffffff";
			   break;
			   
}


echo '<body style="font-size:30">';

	
$MEETING = 41;
$SHOW_SENDER_NAME = 0;
$SHOW_SENDER_PROFILE = 1;
$USE_DEVICE_ID = 0;
$CURRENT_LANGUAGE = "it";

function monthbyid($id) {

global $CURRENT_LANGUAGE;

if($CURRENT_LANGUAGE == "it") {
	
	switch($id) {
	
	case  1: return "gennaio";
	case  2: return "febbraio";
	case  3: return "marzo";
	case  4: return "aprile";
	case  5: return "maggio";
	case  6: return "giugno";
	case  7: return "luglio";
	case  8: return "agosto";
	case  9: return "settembre";
	case 10: return "ottobre";
	case 11: return "novembre";
	case 12: return "dicembre";
	
	}
}

if($CURRENT_LANGUAGE == "en") {
	
	switch($id) {
	
	case  1: return "january";
	case  2: return "february";
	case  3: return "march";
	case  4: return "april";
	case  5: return "may";
	case  6: return "june";
	case  7: return "july";
	case  8: return "august";
	case  9: return "september";
	case 10: return "october";
	case 11: return "november";
	case 12: return "december";
	
	}
}
}


function GetUserNameForDevice($id) {

global $USE_DEVICE_ID;
global $db;

	if($USE_DEVICE_ID) {
		return "iPod ".$id;
	}
	else {
		$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");	
	
		if($row) 
			return $row['slastname']." ".$row['sfirstname'];
		else
			return "iPod non assegnato";
	}
}


function GetUserProfile($id) {

global $USE_DEVICE_ID;
global $db;

	$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");	
	
		if($row) 
			return $row['sprofile'];
		else
			return "";
	
}

function formatdate($date) {

	if($date <> "") {
		
		$year = substr($date,0, 4);
		$month = substr($date,5, 2);
		$day = (int)substr($date, 8, 2);
		$timex = substr($date,13);
		$hour = (int)substr($date, 11, 2)-2;
	//	return $day." ".monthbyid($month)." ".$year." alle ".$hour.$timex;
		
		return $day." ".monthbyid($month)." alle <b>".$hour.$timex."</b>";
	}
	else
		return "";
}

//	echo '<script> setInterval("checkIfRefreshIsNeeded('.$SESSION.')",3000); </script> '; 
	echo '<script> setInterval("checkIfRefreshIsNeeded('.$SESSION.')",3000);</script> '; 
	
	
	//echo "XXXXXXXXXX".$SESSION;
	
	echo  "<form name=\"sessionSelect\" id=\"sessionSelect\" action='".$_SERVER['PHP_SELF']."' method='get'>";

	$resultsq = "SELECT * FROM tblTweetingsessions WHERE fkidMeeting = '".$MEETING."'  ORDER BY iSort";
		
	$results = $db->GetAll($resultsq);	
		
	if(count($results) > 0) {
		
		echo '<div style="width:430px; float:left;" ><select class="selectcustom" name="session" id="session" onChange="sessionSelect.submit();">
		';
		
		echo '<option value="0">&nbsp;&nbsp;Seleziona una sessione di domande</option>
		';
		
		foreach($results as $result) {
	
			echo '<option value="'.$result['pkidTweetingSession'].'"';
			
			if($SESSION == $result['pkidTweetingSession'])
				echo " selected ";
				
			echo '>&nbsp;&nbsp;'.$result['stitle'].'</option>
			';

		}
		echo "</select></div>";
	}
	
	echo '<div><select style="width:160px; margin-left:15px; float:left;" class="selectcustom" name="textsize" id="textsize" onChange="sessionSelect.submit();">
		';
		
	echo '<option value="large"';
	if($TEXTSIZE == "large")
				echo " selected ";
	echo '>&nbsp;&nbsp;Testo grande</option>
			';

	echo '<option value="medium"';
	if($TEXTSIZE == "medium")
				echo " selected ";
	echo '>&nbsp;&nbsp;Testo medio</option>
			';

	echo '<option value="small"';
	if($TEXTSIZE == "small")
				echo " selected ";
	echo '>&nbsp;&nbsp;Testo piccolo</option>
			';
			
	echo "</select></div>";

	
	
	
	echo '<div><select style="width:140px; float:none;" class="selectcustom" name="skin" id="skin" onChange="sessionSelect.submit();">
		';
		
	echo '<option value="dark"';
	if($SKIN == "dark")
				echo " selected ";
	echo '>&nbsp;&nbsp;Buio</option>
			';

	echo '<option value="light"';
	if($SKIN == "light")
				echo " selected ";
	echo '>&nbsp;&nbsp;Luminoso</option>
			';

			
	echo "</select></div>";
	
		
	echo "</form>";
	
	$q = "SELECT * FROM tblTweets WHERE 
		fkidTweetingSession = '".$SESSION."'  
				AND (bdeleted = 0 OR bdeleted IS NULL) AND bApproved = 1  ORDER BY dtmApproved DESC";

	//echo $q;
	
	$rows = $db->GetAll($q);

	if(count($rows) > 0) {
	
		foreach($rows as $row)  {
			
			if(!$row['bShowed'])
				$fontcolor = $color1;
			else
				$fontcolor = $color3;
			
			echo '<div style="';
			echo 'background:'.$color2.';';
			
			echo 'margin:5px; padding:10px; border-width:1px; border-style:dotted; ">';
			
			echo "<font size='".$smallfontsize."' color='".$fontcolor."'>".formatdate($row['dtmApproved'])."</font>";
			
			if($SHOW_SENDER_NAME)
				echo "<font size='".$smallfontsize."' color='".$fontcolor."'>inviato da <b>".GetUserNameForDevice($row['fkidSender'])."</b></font>";
			
			if($SHOW_SENDER_PROFILE) {
			
				$profile = GetUserProfile($row['fkidSender']);
			
				if($profile <> "")
					echo " <font size='".$smallfontsize."' color='".$fontcolor."'>inviato da un <b>".$profile."</b></font>";
				
				
			}
			
			
			echo "<br><br><font size='".$bigfontsize."' color='".$fontcolor."'>".$row['sbody']."</font>";
			
			echo '</div>';	
		}	
				
	}
	
?>
</body>
</html>



