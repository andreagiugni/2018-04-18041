{{if $lang eq "it"}}
{{config_load file="config_it.conf"}}
{{elseif $lang eq "en"}}
{{config_load file="config_en.conf"}}
{{elseif $lang eq "de"}}
{{config_load file="config_de.conf"}}
{{elseif $lang eq "es"}}
{{config_load file="config_es.conf"}}
{{elseif $lang eq "fr"}}
{{config_load file="config_fr.conf"}}
{{else}}
{{config_load file="config_en.conf"}}
{{/if}}
<!DOCTYPE html>
<html lang="en">
<head>
{{include file=$header}}
<link href="css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/ui-darkness/jquery-ui-1.8.16.custom.css" rel="stylesheet"  type="text/css"  />
<style>
		div#users-contain { width: 450px; margin: 20px 0; }
		div#users-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
		div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
		.ui-dialog .ui-state-error { padding: .3em;}
		body{-webkit-touch-callout: none;	-webkit-user-select: none;}
</style>
{{if $browser == 'mobile'}}
<!--<script src="javascript/mobile/iscroll.js"></script>-->
{{/if}}
<script src="javascript/jquery.alerts.js" type="text/javascript"></script>
<script type="text/javascript" src="javascript/jquery-ui-1.8.16.custom.min.js"></script>
<!-- Custom script -->
<script type="text/javascript" src="javascript/scripts.js"></script>
<script>
	$(function() {
		// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
		$( "#dialog:ui-dialog" ).dialog( "destroy" );
						
		$( "#dialog-form" ).dialog({
			autoOpen: false,
			height: 350,
			width: 450,
			modal: true,
			buttons: {
				"Save": function() {
		
					var str=$('#tweet').val(); 
					var id=$('#idtweet').val(); 					
					//Salva					
					saveTweet(id,str);
					$( this ).dialog( "close" );

				},
				Cancel: function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {				
			}
		});

		
	});
</script>
</head>
<body>
{{if $browser != 'mobile'}}
{{include file=$menu}}
<form id="Commands" name="Commands" method="get" action="{{$action}}">
<div class="grid_9">
     <div class="box" style="margin-top:10px;">
              <h2>Approved</h2>	
              <div class="utils">		
                            <a href="javascript:;" id="disableconf" >Disable confirm</a> 
                            <!--<a href="javascript:;" id="hideapproved" >Hide</a>
                            <a href="javascript:;" id="showapproved" >Show</a> &nbsp; &nbsp;             -->                                                                        
                            <a href="javascript:;" id="enableconf" >Enable confirm</a> &nbsp; &nbsp; 	   			                                                                
            </div>
      		<div id="approved">
            </div>
    </div>
</div>

<div class="grid_7">
     <div class="box" style="margin-top:10px;">
              <h2>On Screen</h2>	
              <div class="utils">		
              				<a href="javascript:;" id="clearscreen" >Clear</a> 
                          <!--  
                                <a href="javascript:;" id="hideonscreen" >Hide</a> 
                                <a href="javascript:;" id="showonscreen" >Show</a> &nbsp; &nbsp;   		
                            -->	                                                          
            </div>
             <div id="onscreen">
            </div>
    </div>
</div>

<div class="grid_9">
     <div class="box" style="margin-top:10px;">
              <h2>To approve</h2>	
              <div class="utils">		
                            <!--<a href="javascript:;" id="hidetoapprove" >Hide</a> 
                            <a href="javascript:;" id="showtoapprove" >Show</a> &nbsp; &nbsp;  -->                   
            </div>
            <div id="toapprove">

            </div>
    </div>
</div>

<div class="grid_7"  id="trashbox">
     <div class="box" style="margin-top:10px;">
              <h2>Trash</h2>	
              <div class="utils">		
                            <a href="javascript:;" id="hidetrash" >Hide</a> 
                            <a href="javascript:;" id="showtrash" >Show</a> &nbsp; &nbsp;     		   			                                                           
            </div>
            <div id="trash"></div>
    </div>
</div>
</div> <!--Chiude pagina-->
{{else}}
<div id="main" class="abs">
    <div class="abs header_upper chrome_dark">
        <span class="float_left button" id="button_navigation">
            Navigation
        </span>
        <a href="#" class="float_left button"  id="disableconf" style="color:#FFF;font-size:0.6em;letter-spacing: 0.6px">Disable confirm</a>
        <a href="#" class="float_left button"  id="enableconf" style="color:#FFF;font-size:0.6em;letter-spacing: 0.6px">Enable confirm</a>
                    
    </div>
    <div id="main_content" class="abs">
        <div id="main_content_inner">
            <h3 id="tit_onscreen">
                On Screen
            </h3>        
             <div id="onscreen"></div><br />
             <h3 id="tit_toapprove">
                To Approve
            </h3>                    
            <div id="toapprove"></div>             
            <h3 id="tit_approved">
                Approved
            </h3>                    
            <div id="approved"></div>
            <h3 id="tit_trash">
                Trash
            </h3>                    
            <div id="trash"></div>                          
        </div>
    </div>
<!-- <div class="abs footer_lower chrome_dark"></div> -->
</div>
<div id="sidebar" class="abs">
    <span id="nav_arrow"></span>
    <div class="abs header_upper chrome_dark">TWITTER</div>
   		
    <div id="sidebar_content" class="abs">
        <div id="sidebar_content_inner">
            <ul id="sidebar_menu">
               <!-- <li>
                    <a href="#">Overview</a>
                    <ul>-->
                        <li><a href="#" id="p_toapprove">To Approve</a></li>
                        <li><a href="#" id="p_approved">Approved</a></li>
                        <li><a href="#" id="p_trash">Trash</a></li>
                  <!--  </ul>
                </li> -->
                		<li><a href="postTweet.php?{{$url}}" id="p_domanda">Invio domanda</a></li>
            </ul>
        </div>           
    </div>
<!-- <div class="abs footer_lower chrome_dark"><span class="float_right gutter_right"></span></div> -->
</div>
{{/if}}
<!--<input type="hidden" name="azione" id="azione" value="" />-->
<input type="hidden" name="meeting" id="meeting" value="{{$meetings_id}}" />
<input type="hidden" name="room" id="room" value="{{$rooms_id}}" />
<input type="hidden" name="questionGroup"  id="questionGroup" value="{{$sections_id}}" />
<input type="hidden" name="questionlanguage"  id="questionlanguage" value="{{$langs_id}}" />
<input type="hidden" name="device"  id="device" value="{{$device_id}}" />
<input type="hidden" name="idtweeting"  id="idtweeting" value="{{$idtweet_id}}" />
<input type="hidden" id="alert"   />
</form>
<div id="dialog-form" title="Modify">
	<form>
    <textarea name="tweet" id="tweet"  class="text ui-widget-content ui-corner-all" style="width:405px;height:225px;"></textarea>
    <input type="hidden" name="idtweet"  id="idtweet"  />
	</form>
</div>
<script type="text/JavaScript">  

  $(document).ready(function(){  
		{{if $browser == 'mobile'}}		
			$('#approved').css('display','none');	
			$('#tit_approved').css('display','none');		
			$('#trash').css('display','none');		
			$('#tit_trash').css('display','none');		
			$('#toapprove').css('display','none');	
			$('#tit_toapprove').css('display','none');
	  	{{/if}}
		var timer=3000;   //3 sec.
		var timer01 = setInterval(GetToApprove, 2000);
		var timer02 = setInterval(GetApproved,4000);
		var timer03 = setInterval(GetToScreen, 3000);
		//var timer03 = setTimeout(GetToScreen, 3000);
		var timer04 = setInterval(GetTrash, 6000);
		
		$("#disableconf").click(function(){  
		        $('#alert').val('disable');
                $('#error').removeClass().addClass( 'success');
                $('#error').html('Confirm disabled!');
                $('#error').show(500);    
                var loadTimeout = setTimeout(hideMsg, 4000);
		});		
        
        $("#enableconf").click(function(){  
                $('#alert').val('enable');
                $('#error').removeClass().addClass( 'success');
                $('#error').html('Confirm enabled!');
                $('#error').show(500);    
                var loadTimeout = setTimeout(hideMsg, 4000);
        });
        
        $("#clearscreen").click(function(){  
            ClearScreen();        
        });    	
            
		$("#hideapproved").click(function(){  
			$('#approved').css('display','none');			
		});		
		$("#showapproved").click(function(){  
			$('#approved').css('display','block');			
		});	
		$("#hideonscreen").click(function(){  
			$('#onscreen').css('display','none');			
		});		
		$("#showonscreen").click(function(){  
			$('#onscreen').css('display','block');			
		});        
		$("#hidetoapprove").click(function(){  
			$('#toapprove').css('display','none');			
		});		        
		$("#showtoapprove").click(function(){  
			$('#toapprove').css('display','block');			
		});
		$("#hidetrash").click(function(){  
			$('#trash').css('display','none');			
            //$('#trashbox').css('display','none');   
            //$('#onscreen').css('height','auto');                     
            
		});	
		$("#showtrash").click(function(){  
			$('#trash').css('display','block');			
            // $('#trashbox').css('display','block');   
		});
		
		
		$("#p_toapprove").click(function(){  
			$('#tit_approved').css('display','none');		
			$('#approved').css('display','none');		
			$('#tit_trash').css('display','none');	
			$('#trash').css('display','none');		
			$('#tit_toapprove').css('display','block');
			$('#toapprove').css('display','block');	
			$("#p_toapprove").addClass( 'active');
			
		});
		$("#p_approved").click(function(){  
			$('#tit_approved').css('display','block');	
			$('#approved').css('display','block');	
			$('#tit_trash').css('display','none');	
			$('#trash').css('display','none');	
			$('#tit_toapprove').css('display','none');
			$('#toapprove').css('display','none');	
			$("#p_approved").addClass( 'active');
			
		});
		$("#p_trash").click(function(){  
			$('#tit_approved').css('display','none');	
			$('#approved').css('display','none');	
			$('#tit_trash').css('display','block');		
			$('#trash').css('display','block');	
			$('#tit_toapprove').css('display','none');	
			$('#toapprove').css('display','none');	
			$("#p_trash").addClass( 'active');
			
		});	
  });  
</script> 
</body>
</html>