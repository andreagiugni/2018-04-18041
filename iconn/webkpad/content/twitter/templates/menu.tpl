{{if $browser != 'mobile'}}
<h1 id="head">{{$title_site}}</h1>
<ul id="navigation">
        <li><a href="index.php?{{$url}}" class="{{$active01}}">{{#menu_overview#}}</a></li>
        <li><a href="postTweet.php?{{$url}}" class="{{$active02}}">{{#menu_twitter_sendtweet#}}</a></li>      
  
</ul>
<div id="content" class="container_16 clearfix">
<div class="grid_16">
    <h2 style="font-size:1.6em;">{{$title_page}}</h2>
    <div class="box" id="history" >&nbsp;&nbsp;<strong>{{#history_session#}}:</strong>&nbsp;&nbsp;{{$meeting}}&nbsp;{{if $meeting != ''}}&raquo;{{/if}}&nbsp;{{$room}}&nbsp;{{if $room != ''}}&raquo;{{/if}}&nbsp;{{$session}}</div>
    <p class="error" id="error" style="display:none;"></p>
</div>
{{/if}}
