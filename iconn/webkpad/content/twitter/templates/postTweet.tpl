{{if $lang eq "it"}}
{{config_load file="config_it.conf"}}
{{elseif $lang eq "en"}}
{{config_load file="config_en.conf"}}
{{elseif $lang eq "de"}}
{{config_load file="config_de.conf"}}
{{elseif $lang eq "es"}}
{{config_load file="config_es.conf"}}
{{elseif $lang eq "fr"}}
{{config_load file="config_fr.conf"}}
{{else}}
{{config_load file="config_en.conf"}}
{{/if}}
<!DOCTYPE html>
<html lang="en">
<head>
{{include file=$header}}
<!-- Custom script -->
<script type="text/javascript" src="javascript/scripts.js"></script>
</head>
<body>
<form id="Commands" name="Commands" method="get" onSubmit="javascript:return false;">
{{if $browser != 'mobile'}}
{{include file=$menu}}
<div class="grid_16">
      <p>
          <label>{{#lbl_messaggio#}}</label>
          <textarea class="big" id="msg" name="msg" style="height:200px;"></textarea>
      </p>
      <p class="submit">
          <input type="reset" value="Back"  id="back"/>
          <input type="submit" value="Send"  id="send"/>
      </p>
</div>
{{else}}
<div id="main" class="abs">
    <div class="abs header_upper chrome_dark">
        <span class="float_left button" id="button_navigation">
            Navigation
        </span>
        <a href="index.php?{{$url}}" class="float_left button"  id="p_back" style="color:#FFF;">Back</a>
    </div>
    <div id="main_content" class="abs">
        <div id="main_content_inner">
               <p>
                  <label style="font-size:1.2em;">{{#lbl_messaggio#}}:</label>
                  <textarea  id="msg" name="msg" rows="5" ></textarea>
              </p>              
              <p >                 
                  <input type="submit" value="Send"  id="send" style="width:90px;height:30px;font-size:1.2em;"/>
              </p>
        </div>
    </div>
</div>
<div id="sidebar" class="abs">
    <span id="nav_arrow"></span>
    <div class="abs header_upper chrome_dark">TWITTER</div>
   		
    <div id="sidebar_content" class="abs">
        <div id="sidebar_content_inner">
            <ul id="sidebar_menu"  >
           		<li id="sidebar_menu_home" class="active"><a href="postTweet.php?{{$url}}" id="p_domanda">Invio domanda</a></li>
            </ul>
        </div>           
    </div>
</div>


{{/if}}
<input type="hidden" name="meeting" id="meeting" value="{{$meetings_id}}" />
<input type="hidden" name="room" id="room" value="{{$rooms_id}}" />
<input type="hidden" name="questionGroup"  id="questionGroup" value="{{$sections_id}}" />
<input type="hidden" name="questionlanguage"  id="questionlanguage" value="{{$langs_id}}" />
<input type="hidden" name="idtweeting" id="idtweeting" value="{{$tweetingsession_id}}" />
</form>
<script type="text/JavaScript">  
  $(document).ready(function(){  
      
      function saveTweet(body)
      {
          var room = $('#room').val();
          var meeting = $('#meeting').val();
          var questionlang =  $('#questionlanguage').val();
          var questiongroup =  $('#questionGroup').val();
          var idtweeting =  $('#idtweeting').val();
         
		 $.ajax({
                type : 'GET',
                url : 'insertTweet.php',                            
                data: {
                     testo:body, room:room, meeting:meeting, questionlanguage:questionlang, questionGroup:questiongroup, idtweeting:idtweeting
                },
                dataType: "html",
                success : function(data,stato){                   
				    //alert("Data Loaded: " + data);
//                    $('#error').removeClass().addClass((data.error === true) ? 'error' : 'success')
//                    $('#error').html(data);
//                    $('#error').show(500);    
//                    var loadTimeout = setTimeout(hideMsg, 5000);
					window.location.href ='index.php?{{$url}}';
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                   $('#error').removeClass().addClass('error')
                    $('#error').html("Error to insert!");
                    $('#error').show(500);     
                }
        });
        return false;
      }
      
      $("#send").click(function(){       
              var body=$('#msg').val(); 
              if($.trim(body)=="")  return;
              
			  
              saveTweet(body);  
        });
        
        
        
  });  
</script> 
</body>
</html>