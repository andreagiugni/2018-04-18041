<style>

body {

	font-size:8px;
	font-family:arial;
}

</style>

<body>


<?php

include("connect.php");







function ShowQuestion($i) {
	
global $db;

	$q = "SELECT iAnswer, count(iAnswer) as c FROM tblVirtualPatientAction WHERE fkidPatient = '1' AND iTest = '".$i."' GROUP BY iAnswer";
	
	//echo $q."<br>";
	
	echo "<h2>".LookupQuestions($i)."</h2>";
	
	$rows = $db->GetAll($q);	
	
	foreach($rows as $row) {
		
		echo $row['c']." - ".LookupAnswers($row['iAnswer'])."<br>";		
	}
	
	echo "<br>";
	
}

echo "<h1>JUAN VIRTUAL PATIENT</h1>";

echo "<h1>Test your knowledge</h1>";

ShowQuestion(1);
ShowQuestion(2);
ShowQuestion(3);


function LookupQuestions($i) {

switch($i) {
		
	case 1: return "Biologics are absolutely contraindicated in a patient with a history of treated tuberculosis:";
			break;
	
	case 2: return "For baseline TB screening, the IGRA (interferon-gamma release assay) offers superior sensitivity and specificity compared to the Mantoux test:";
			break;
	
	case 3: return "A patient with suspected latent TB at screening:";
			break;
	
	}

}


function LookupAnswers($i) {

	switch($i) {
		
	case 99: return "True";
			break;
			
	case 100: return "False";
			break;
			
	case 101: return "True";
			break;
			
	case 102: return "False";
			break;
		
	case 103: return "Should not receive biologic therapy";
			break;
	
	case 104: return "Must complete a full course of anti-tuberculosis therapy before biologic therapy can be considered";
			break;
			
	case 105: return "Must be started with anti-tuberculosis therapy before the initiation of a biologic, and in accordance with local regulations";
			break;
	}
}



?>
</body>
